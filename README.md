# Blenheim Chalcot Test



## Getting started
- Install Node modules
```
 npm install
```
- Build project files

```
npm run build
```

- Run tests
```
npm run test
```
