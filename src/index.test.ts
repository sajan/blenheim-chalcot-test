
import { accountTypeChecker,IAccountBalanceHistory } from "./index"


describe('accountTypeChecker', () => {
    test('Basic', () => {
        const test1: IAccountBalanceHistory[] = [
            {
                monthNumber: 0, // current month
                account: {
                    balance: { amount: 0 },
                },
            },
            {
                monthNumber: 1, // last month
                account: {
                    balance: { amount: 100 },
                },
            },
            {
                monthNumber: 2, // two months ago
                account: {
                    balance: { amount: 200 },
                },
            }
        ]
    
        expect(accountTypeChecker(test1)).toBe('B');
    });
    
    test('Basic Variable Change', () => {
        const test1: IAccountBalanceHistory[] = [
            {
                monthNumber: 0, // current month
                account: {
                    balance: { amount: 0 },
                },
            },
            {
                monthNumber: 1, // last month
                account: {
                    balance: { amount: 1100 },
                },
            },
            {
                monthNumber: 2, // two months ago
                account: {
                    balance: { amount: 200 },
                },
            }
        ]
    
        expect(accountTypeChecker(test1)).toBe('A');
    });
    
    test('Months are not in order', () => {
        const test1: IAccountBalanceHistory[] = [
            {
                monthNumber: 1, // last month
                account: {
                    balance: { amount: 100 },
                },
            },
            {
                monthNumber: 0, // current month
                account: {
                    balance: { amount: 0 },
                },
            },
            {
                monthNumber: 2, // two months ago
                account: {
                    balance: { amount: 200 },
                },
            }
        ]
    
        expect(accountTypeChecker(test1)).toBe('B');
    });
    
    test('Basic Change (With increase amount)', () => {
        const test1: IAccountBalanceHistory[] = [
            {
                monthNumber: 0, // current month
                account: {
                    balance: { amount: 2000 },
                },
            },
            {
                monthNumber: 1, // last month
                account: {
                    balance: { amount: 1100 },
                },
            },
            {
                monthNumber: 2, // two months ago
                account: {
                    balance: { amount: 200 },
                },
            }
        ]
    
        expect(accountTypeChecker(test1)).toBe('B');
    });
    
    test('Basic Variable Change (With increase amount)', () => {
        const test1: IAccountBalanceHistory[] = [
            {
                monthNumber: 0, // current month
                account: {
                    balance: { amount: 2000 },
                },
            },
            {
                monthNumber: 1, // last month
                account: {
                    balance: { amount: 1500 },
                },
            },
            {
                monthNumber: 2, // two months ago
                account: {
                    balance: { amount: 200 },
                },
            }
        ]
    
        expect(accountTypeChecker(test1)).toBe('A');
    });
}) 
