export interface IAccountBalanceHistory {
    monthNumber: number, // current month
    account: {
        balance: {
            amount: number
        },
    },
}

export const accountTypeChecker = (accountBalanceHistory: IAccountBalanceHistory[]): String => {
    /***
    Your goal is to write a function that determines from someone's ${accountBalanceHistory} 🧾 (see the array above for an example)
    whether this array is of type A (variable) or type B (fixed).
  
    Type 🅰 denotes a balance history where the balance amount decreases by varying amounts each month.
  
    Type 🅱 is one where the balance amount changes by the same amount each month.
    ***/

    // Write your logic here  - No pressure 😁 //
    let difference = null;
    let i = 0;
    let j = 1;
    let result = false;
    // sorting balance history by month
    const sortedAccountBalanceHistory = accountBalanceHistory.sort((a, b) => a.monthNumber - b.monthNumber)
    while (j < sortedAccountBalanceHistory.length) {
        let currentDifference = Math.abs(sortedAccountBalanceHistory[i].account.balance.amount - sortedAccountBalanceHistory[j].account.balance.amount);

        // console.log({
        //     i, j,
        //     iAmount: sortedAccountBalanceHistory[i].account.balance.amount,
        //     jAmount: sortedAccountBalanceHistory[j].account.balance.amount,
        //     currentDifference,
        //     difference
        // })

        if (difference == null) {
            difference = currentDifference;
        }
        else if (currentDifference !== difference) {
            result = true;
            break;
        }
        i++;
        j++;
    }
    return result ? "A" : "B";
};


